<?php

use Illuminate\Http\Request;
Use App\Inquiry;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::get('inquiry', function(){
//     Mail::send('gg', ['name' => 'Joses'], function($message){
//         $message->to('cyriel.bas@gmail.com', 'Its me')->subject('welcome');
//     });
// });
Route::post('inquiry', 'InquiryController@store');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

